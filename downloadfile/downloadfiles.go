package main

import (
	"crypto/tls"
	"fmt"
	"net/http"
	"os"
	"io"
	"strings"
	"bytes"
)

func main(){
	fileWithIds, err := os.Open("downloadfiles.txt")
	check(err)

	buf := bytes.NewBuffer(nil)

	io.Copy(buf, fileWithIds)
	fileWithIds.Close()
	fileIds := string(buf.Bytes())

	for _,element := range strings.Split(fileIds, "\r\n") {
		downloadURL(element)
	}
}

func downloadURL(fileId string) {
	getURL := "https://search.softserveinc.com/api/employees/" + fileId + "/photo?source=card"
	fileName := fileId + ".jpg"
	folder := "downloadfiles/"
	// Create New http Transport
	transCfg := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true}, // disable verify
	}
	// Create Http Client
	client := &http.Client{Transport: transCfg}
	// Request
	response, err := client.Get(getURL)
	// Check Error
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	file, err := os.Create(folder + fileName)

	if err != nil {
		fmt.Println(err)
		panic(err)
	}
	defer file.Close()

	size, err := io.Copy(file, response.Body)

	if err != nil {
		panic(err)
	} else {
		fmt.Println(fileName, "downloaded, size:", size)
	}
	// Close After Read Body
	defer response.Body.Close()
}


func check(e error) {
	if e != nil {
		panic(e)
	}
}