package main

import (
	"fmt"
)

func main() {
	for i := 1; i <= 4; i++ {
		for j := 1; j <= 4; j++ {
			for k := 1; k <= 4; k++ {
				for l := 1; l <= 4; l++ {
					if i != j && i != k && i != l && j != k && j != l && k != l {
						fmt.Println(i, ",", j, ",", k, ",", l)
					}
				}
			}
		}
	}
}