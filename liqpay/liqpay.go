package liqpay

import (
	"net/http"
	"fmt"
	"log"
	"encoding/json"
	"encoding/base64"
	"crypto/sha1"
)

const (
	API_VERSION = "3"
	PUBLIC_KEY = "i52048943650"
	PRIVATE_KEY = "QebfQQTgb6gUHUh0crT2xZth1qOACyjUjFgTzLnu"
	ACTION = "pay"
	CURRENCY = "USD"
	TEST_AMOUNT = "0.01"
	TEST_DESCRIPTION = "test test"
	TEST_ORDER_ID = "123"
)

/*

$params = array('version' => 3,
                'public_key' => 'i52048943650',
                'action' => 'pay',
                'amount' => '0.01',
                'currency' => 'USD',
                'description' => 'test test',
                'order_id' => '123'
                );

$json_string = json_encode($params);

$private_key = 'QebfQQTgb6gUHUh0crT2xZth1qOACyjUjFgTzLnu';
$data = base64_encode( $json_string );
$signature = base64_encode( sha1( $private_key . $data . $private_key, 1 ) );

echo 'data:<br>' . $data;
echo '<br><br>signature:<br>' . $signature;

<form method="POST" action="https://www.liqpay.com/api/3/checkout"
      accept-charset="utf-8">
    <input type="hidden" name="data" value="eyJ2ZXJzaW9uIjozLCJwdWJsaWNfa2V5IjoiaTUyMDQ4OTQzNjUwIiwiYWN0aW9uIjoicGF5IiwiYW1vdW50IjoiMC4wMSIsImN1cnJlbmN5IjoiVVNEIiwiZGVzY3JpcHRpb24iOiJ0ZXN0IHRlc3QiLCJvcmRlcl9pZCI6IjEyMyJ9"/>
    <input type="hidden" name="signature" value="ij20nik+A5zdrGwiVnVI7DzH7a8="/>
    <input type="image" src="//static.liqpay.com/buttons/p1en.radius.png"/>
</form>


////PHP extended:
<?php
$params = array(
'action' => 'pay',
'amount' => '0.01',
'currency' => 'USD',
'description' => 'test test',
'order_id' => '123',
'public_key' => 'i52048943650',
'version' => "3"
);

$json_string = json_encode($params);
$private_key = 'QebfQQTgb6gUHUh0crT2xZth1qOACyjUjFgTzLnu';
$data = base64_encode( $json_string );
$str = $private_key . $data . $private_key;
$sha1 = sha1( $str, 1 );
$signature = base64_encode( $sha1 );
?>

<br>json encoded:
<br><?php echo $json_string; ?>

<br>{{jsonencoded}}
<br>
<br>base64 encoded json string (data itself):
<br><?php echo $data; ?>

<br>{{base64encodedjson}}
<br>
<br>string for signature:
<br><?php echo $str; ?>

<br>{{signaturestring}}
<br>
<br>string for signature sha1 encoded:
<br><?php echo $sha1; ?>

<br>{{signaturestringsha}}
<br>
<br>string for signature sha1 encoded and base64 encoded (signature itself):
<br><?php echo $signature; ?>

<br>{{signaturestringshabase64}}

 */


func main() {
	params := map[string]string{
		"version": API_VERSION,
		"public_key": PUBLIC_KEY,
		"action": ACTION,
		"amount": TEST_AMOUNT,
		"currency": CURRENCY,
		"description": TEST_DESCRIPTION,
		"order_id": TEST_ORDER_ID,
	}

	jsonString, _ := json.Marshal(params)

	//fmt.Print(string(jsonString)) // - ok

	data := base64.StdEncoding.EncodeToString([]byte(string(jsonString)))

	//d,_ := base64.StdEncoding.DecodeString(data)
	//fmt.Print(string(d)) // - ok

	data = PRIVATE_KEY + data + PRIVATE_KEY

	hash := sha1.New()
	hash.Write([]byte(data))

	dataEncoded := hash.Sum(nil)
	signature := base64.StdEncoding.EncodeToString([]byte(dataEncoded))

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		//fmt.Fprintf(w, "Hello, %q", html.EscapeString(r.URL.Path))
		fmt.Fprintf(w, "data: " + data + "\nsignature: " + signature)
	})

	log.Fatal(http.ListenAndServe(":3000", nil))
}