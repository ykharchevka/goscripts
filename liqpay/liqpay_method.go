// Deprecated: will stop using once G2A is working well
func handlePaymentLiqpay(w http.ResponseWriter, r *http.Request) {
	params := map[string]string{
		"version":    "3",
		"public_key": "i52048943650",
		"action":     "pay",
		//"amount": "0.01",
		"amount":      r.FormValue("cost"),
		"currency":    "USD",
		"description": r.FormValue("name") + " for " + r.FormValue("contact"),
		"order_id":    string(time.Now().UnixNano()),
	}

	page, err := ioutil.ReadFile("html/elements/payformLiqpay.html")
	if err != nil {
	}
	pageString = string(page)
	jsonString, _ := json.Marshal(params)
	data := base64.StdEncoding.EncodeToString([]byte(string(jsonString)))
	pageString = strings.Replace(pageString, "{{data}}", data, -1)

	dataString := "QebfQQTgb6gUHUh0crT2xZth1qOACyjUjFgTzLnu" + data + "QebfQQTgb6gUHUh0crT2xZth1qOACyjUjFgTzLnu"
	dataEncoded := sha1.Sum([]byte(dataString))
	signature := base64.StdEncoding.EncodeToString(dataEncoded[:20])
	pageString = strings.Replace(pageString, "{{signature}}", signature, -1)
	http.Redirect(w, r, "https://www.liqpay.com/api/3/checkout?data="+data+"&signature="+signature, 301)
}